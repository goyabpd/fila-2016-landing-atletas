;(function () {
    'use strict';

    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }

    var app = angular.module('app', []);

    app.controller('main-controller', function($scope, $http, $timeout) {

        var $modal = $('.modal');
        var $emptyThummb = $('<div class="thumb thumb--empty" />');
        var $currentThumb;

        var THUMBS_PER_LINE = 3;

        $scope.title = 'GOYA Boilerplate';
        $scope.modalActive = false;
        $scope.accordionTennisOpen = true;
        $scope.accordionRunnerOpen = true;
        $scope.thumbs = [];
        $scope.currentData = {};
        $scope.modalHeight = 615;

        $emptyThummb.height($scope.modalHeight);

        $scope.imgSrc = "aaa";

        $scope.closeTennisAccordion = "-";
        $scope.closeRunnerAccordion = "-";

        $http.get('data.json').success(function (data, status) {
            $scope.thumbs = data;
        });

        var setModalPosition = function ($element) {

            var thumbElementTop = $element.offset().top;
            var thumbHeight = $element.outerHeight(true);

            /*console.log('modalHeight:', $scope.modalHeight);
            console.log('thumbElementTop:', thumbElementTop);
            console.log('thumbHeight:', thumbHeight);
            console.log('modalOpenDifference:', modalOpenDifference);
            console.log('(thumbElementTop + thumbHeight + modalOpenDifference):', (thumbElementTop + thumbHeight + modalOpenDifference));*/

            $modal.css('top', (thumbElementTop + thumbHeight) + 'px');

            scrollTo((thumbElementTop + thumbHeight) + 500, function () {
                console.log('scroll callback');
            })

        };

        var insertEmptyElement = function ($element) {
            var elementIndex = $element.index();
            var elementLine = Math.ceil((elementIndex + 1) / THUMBS_PER_LINE);
            var $thumbsContainer = $element.closest('.thumbs-container');

            $thumbsContainer.find('.thumb').eq((elementLine * THUMBS_PER_LINE)-1).after($emptyThummb);
            /*console.log($thumbsContainer.find('.thumb'));
            console.log((elementLine * THUMBS_PER_LINE));
            console.log($thumbsContainer.find('.thumb').eq((elementLine * THUMBS_PER_LINE)-1));*/
        }

        $scope.openModal = function ($event, id) {
            $currentThumb = $($event.target).closest('.thumb');
            $emptyThummb.removeClass('show').remove();

            insertEmptyElement($currentThumb);
            setModalPosition($currentThumb);

            $timeout(function() {
                $emptyThummb.addClass('show');
                $modal.addClass('show');
            }, 100);

            $scope.currentData = _.first(_.where($scope.thumbs, {"id": id}));
            $scope.currentData.image = $scope.currentData.images[0];
            $scope.modalActive = true;


            if($scope.currentData.images.length <= 5){
                $scope.modalHeight = 550;
            }else{
                $scope.modalHeight = 640;
            };

            $emptyThummb.height($scope.modalHeight);

            var screenHalf = $(window).height()/2;
            var modalHalf = $('#modal').height()/2;

            $('html, body').animate({
                scrollTop: $("#modal").offset().top-screenHalf+300
            }, 150);


        };

        $scope.closeModal = function () {
            $emptyThummb.removeClass('show').remove();
            $modal.removeClass('show');
            $scope.modalActive = false;
            $scope.currentData = {};
        };

        var scrollTo = function (Y, duration, callback) {

            console.log('scrollTo');
            console.log('Y', Y);

            var start = Date.now(),
                elem = document.documentElement.scrollTop ? document.documentElement : document.body,
                from = elem.scrollTop;

            if (navigator.userAgent.toLowerCase().indexOf('msie') !== -1 || navigator.userAgent.toLowerCase().indexOf('trident') !== -1) {
                elem = document.documentElement;
            }

            if (typeof InstallTrigger !== 'undefined') {
                elem = document.documentElement;
            }

            callback = typeof callback === 'function' ? callback : function () {};

            if(from === Y) {
                callback();
                return; /* Prevent scrolling to the Y point if already there */
            }

            function min(a,b) {
                return a<b?a:b;
            }

            function easingFunction(t) { return (--t)*t*t+1; }

            function scroll(timestamp) {

                var currentTime = Date.now(),
                    time = min(1, ((currentTime - start) / duration)),
                    easedT = easingFunction(time);

                elem.scrollTop = (easedT * (Y - from)) + from;

                if(time < 1) {
                    requestAnimationFrame(scroll);
                } else {
                    if(callback) {
                        callback();
                    }
                }
            }

            requestAnimationFrame(scroll);
        };
    });




    app.filter('category', function() {
        return function(data, category){
            return _.where(data, { category : category });
        }
    });
})();