;(function () {
    'use strict';

    var app = angular.module('app', []);
    var parentURL = document.location.hostname === 'localhost' ? 'http://localhost/~moises/fila_landing/' : 'http://www.fila.com.br/atletas';

    app.controller('main-controller', function($scope, $http, $timeout) {

        var $window = $(window);
        var $document = $(document);
        var $html = $('html');
        var $modal = $('.modal');
        var $currentThumb = false;
        var tempCurrentData = false;

        var THUMBS_PER_LINE = 3;
        var THUMB_EXTRA_SPACE = 40;
        var THUMB_DEFAULT_MARGIM_BOTTOM = 20;

        $scope.modalActive = false;
        $scope.modalTop = '0';
        $scope.accordionTennisOpen = true;
        $scope.accordionRunnerOpen = true;
        $scope.thumbs = [];
        $scope.currentData = false;
        $scope.modalHeight = 615;
        $scope.thumbOffsetTop = 0;
        $scope.thumbHeight = 0;

        // $emptyThummb.height($scope.modalHeight);

        $scope.imgSrc = "aaa";

        $scope.closeTennisAccordion = "-";
        $scope.closeRunnerAccordion = "-";

        $scope.$watch('modalTop', function() {
            console.log('modalTop', $scope.modalTop, arguments);
        });

        $scope.$watch('thumbHeight', function() {
            console.log('thumbHeight', $scope.thumbHeight, arguments);
        });


        $scope.getSocialClass = function (link, index) {
            var network = '';

            if (link.indexOf('facebook.com') > -1) {
                network = 'facebook';
            } else if (link.indexOf('youtube.com') > -1) {
                network = 'youtube';
            } else if (link.indexOf('twitter.com') > -1) {
                network = 'twitter';
            } else if (link.indexOf('instagram.com') > -1) {
                network = 'instagram';
            }

            return 'social-' + network;
        };

        $http.get('http://www.news.fila.com.br/landing_atletas/public/scripts/data.json').success(function (data, status) {
        // $http.get('public/scripts/data.json').success(function (data, status) {
            $scope.thumbs = data;
        });

        var setModalPosition = function ($element) {
            console.log('setModalPosition - modalTop');
            console.log('setModalPosition - thumbHeight');
            $scope.modalTop = ($scope.thumbOffsetTop + $scope.thumbHeight) + 'px';
        };

        var scrollTo = function () {
            console.log('scrollTo - thumbHeight');
            var scrollTop = parseInt(($scope.thumbOffsetTop + $scope.thumbHeight), 10);

            // console.log("$scope.thumbOffsetTop:", $scope.thumbOffsetTop);
            // console.log("$scope.thumbHeight:", $scope.thumbHeight);
            // console.log("$scope.modalHeight:", $scope.modalHeight);
            // console.log("($scope.modalHeight / 2):", ($scope.modalHeight / 2));
            // console.log("scrollTop:", scrollTop);

            if (parent === window) {
                $timeout(function () {
                    $('html, body').animate({
                        scrollTop: scrollTop + 'px'
                    }, 300);
                }, 200);
            } else {
                // parent.postMessage(scrollTop + '|' + $scope.modalHeight, 'http://www.fila.com.br/atletas');
                parent.postMessage(scrollTop + '|' + $html.height(), parentURL);
            }
        };

        var windowResize = function () {
            console.log('windowResize()');
            if ($scope.modalActive) {
                $scope.thumbOffsetTop = $currentThumb.offset().top;
                $scope.thumbHeight = $currentThumb.outerHeight() + THUMB_DEFAULT_MARGIM_BOTTOM; // 20 = default margim bottom
                $scope.modalHeight = $modal.outerHeight(true);

                $currentThumb.css('margin-bottom', ($scope.modalHeight + THUMB_EXTRA_SPACE) + 'px');
                setModalPosition();
            } else {
                console.log('windowResize() - else', $html.height());
                parent.postMessage('0|' + $html.height(), parentURL);
            }
        };

        $scope.openModal = function ($event, id) {
            // console.log('openModal');
            $scope.closeModal();

            tempCurrentData = _.first(_.where($scope.thumbs, {"id": id}));

            if (tempCurrentData && $scope.currentData && tempCurrentData.id === $scope.currentData.id) {
                return false;
            }

            $scope.currentData = tempCurrentData;
            $scope.currentData.image = $scope.currentData.images[0];
            $scope.modalActive = true;

            $currentThumb = $($event.target).closest('.thumb');

            $window.trigger('resize');

            $timeout(function() {
                $modal.addClass('show');
                scrollTo();
            }, 250);
        };

        $scope.closeModal = function () {
            // console.log('closeModal');
            if (!$scope.modalActive || !$currentThumb) {
                return false;
            }

            $currentThumb.css('margin-bottom', '20px');
            $modal.removeClass('show');
            $scope.modalActive = false;
            $scope.currentData = {};

            if (parent !== window) {
                parent.postMessage('0|' + $html.height(), parentURL);
            }
        };

        $window.smartresize(windowResize);

        $timeout(function () {
            console.log('window resize timeout');
            windowResize();
        });
    });


    app.filter('category', function() {
        return function(data, category){
            return _.where(data, { category : category });
        };
    });

    $(function () {
        if (parent !== window) {
            $('html').addClass('iframe');
        }
    });
})();
