<!doctype html>
<html class="no-js" lang="" ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>DASS</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="http://www.news.fila.com.br/landing_atletas/public/styles/main.css">
        <!-- <link rel="stylesheet" href="public/styles/main.css"> -->
    </head>

    <body >
        <div class="atletas-landing" ng-controller="main-controller">
            <img style="width: 100%" alt="" src="http://www.news.fila.com.br/landing_atletas/atletas/main-image.jpg" />
            <div class="breadcrumbs-custom">
                <div class="wrapper">
                    <ul>
                        <li class="home">
                            <a target="_top" href="http://www.fila.com.br/" title="Ir p/ Página Inicial">Início</a>
                            <span>&gt; </span>
                        </li>
                        <li class="cms_page">
                            <strong>Atletas FILA</strong>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="modal" class="modal" ng-model="athletId" ng-style="{'top': modalTop}">
                <div class="wrapper">
                    <div class="modal-close" ng-click="closeModal()"></div>
                    <div class="left">
                        <div class="img-box main">
                            <img ng-src="{{currentData.image}}">
                        </div>
                        <div class="img-box tiny" ng-repeat="image in currentData.images track by $index" ng-click="currentData.image = image;">
                            <a href="" class="hover"><span></span></a>
                            <img ng-src="{{image}}">
                        </div>
                    </div>
                    <div class="right">
                        <div class="top-info">
                            <div class="social-name">
                                <h1 class="name">{{currentData.title}}</h1>
                                <ul class="social" ng-hide="currentData.category == 'runner'">
                                    <li ng-repeat="link in currentData.socialLinks track by $index"> <a target="_blank" ng-class="getSocialClass(link, $index)" href="{{link}}"></a></li>
                                </ul>
                            </div>
                            <span class="sep" ng-hide="currentData.category == 'runner'"></span>
                            <div class="ranking" ng-hide="currentData.category == 'runner'"><span class="big">{{currentData.rank}}</span><span >Singles Ranking</span></div>
                            <span class="sep" ng-hide="currentData.category == 'runner'"></span>
                            <a target="blank" href="{{currentData.brandLink}}" ng-hide="currentData.category == 'runner'"><img class="brand" ng-src="{{currentData.brand}}"></a>
                        </div>

                        <div class="bio">
                            <h2 class="title">Biografia</h2>
                            <p>{{currentData.bio}}</p>
                        </div>

                        <div class="main-info" ng-hide="currentData.category == 'runner'">
                            <h2 class="title">Informações</h2>
                            <div class="left">
                                <div class="item">
                                    <h3 class="info">Nacionalidade:</h3><span class="info">{{currentData.birthplace}}</span>
                                </div>
                                <div class="item">
                                    <h3 class="info">Aniversário:</h3><span class="info">{{currentData.dateOfBirth}}</span>
                                </div>
                                <div class="item">
                                    <h3 class="info">Altura:</h3><span class="info">{{currentData.heigh}}</span>
                                </div>
                            </div>
                            <div class="right">
                                <div class="item">
                                    <h3 class="info">Jogada:</h3><span class="info">{{currentData.plays}}</span>
                                </div>
                                <div class="item">
                                    <h3 class="info">Status:</h3><span class="info">{{currentData.status}}</span>
                                </div>
                                <div class="item">
                                    <h3 class="info">Peso:</h3><span class="info">{{currentData.weight}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wrapper">
            <div class="main-text">
                <h1>Atletas FILA</h1>

                <p>A Fila passou os últimos 50 anos desenvolvendo produtos para os amantes do esporte. O primeiro contato com os esportes, foram nos anos 60, quando a marca começou a patrocinar tenistas, alcançando o auge alguns anos depois, quando o esporte chegou a sua época de ouro. Mais tarde, a Fila patrocinou atletas de basquete, pilotos de Fórmula1 e de moto velocidade, além dos atletas de corrida performance. Desde seu inicio a Fila aliou estilo, conforto e alta tecnologia, para ajudar os melhores atletas de cada segmento.</p>
            </div>

            <a href="" class="accordion-button">Running<span class="close-modal ng-cloak ng-cloak" ng-click="accordionRunnerOpen = !accordionRunnerOpen; closeRunnerAccordion = accordionRunnerOpen ? '-' : '+'">{{closeRunnerAccordion}}</span></a>

            <div class="thumbs-container" ng-class="{'open': accordionRunnerOpen}">
                <div ng-repeat="thumb in thumbs | category: 'runner'" class="thumb" ng-click="openModal($event, thumb.id);" ng-class="{'active': currentData.id == thumb.id}">
                    <div class="img-box">
                        <a href="" class="hover"><span>Ver detalhes</span></a>
                        <img ng-src="{{thumb.images[0]}}">
                    </div>
                    <h1 class="title">{{thumb.title}}</h1>
                    <h2 class="subtitle">{{thumb.subtitle}}</h2>
                </div>
            </div>

            <a href="" class="accordion-button">Tennis<span class="close-modal ng-cloak" ng-click="accordionTennisOpen = !accordionTennisOpen; closeTennisAccordion = accordionTennisOpen ? '-' : '+'">{{closeTennisAccordion}}</span></a>


            <div class="thumbs-container" ng-class="{'open': accordionTennisOpen}">
                <div ng-repeat="thumb in thumbs | category: 'tennis'" class="thumb" ng-click="openModal($event, thumb.id);" ng-class="{'active': currentData.id == thumb.id}">
                    <div class="img-box">
                        <a href="" class="hover"><span>Ver detalhes</span></a>
                        <img ng-src="{{thumb.images[0]}}">
                    </div>
                    <h1 class="title">{{thumb.title}}</h1>
                    <h2 class="subtitle">{{thumb.subtitle}}</h2>
                </div>
            </div>
        </div>
    </div>

        <script type="text/javascript" src="http://www.fila.com.br/js/lib/jquery/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="http://www.news.fila.com.br/landing_atletas/public/scripts/vendor.js"></script>
        <script type="text/javascript" src="http://www.news.fila.com.br/landing_atletas/public/scripts/all.js?v=3"></script>
        <!-- <script type="text/javascript" src="public/scripts/vendor.js"></script> -->
        <!-- <script type="text/javascript" src="public/scripts/all.js"></script> -->
    </body>
</html>
