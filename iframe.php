    <script type="text/javascript" src="http://www.fila.com.br/js/lib/jquery/jquery-1.10.2.min.js"></script>
    <script>
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

        var $iframe;


        var scrollTo = function (elementTop) {
            if (elementTop) {
                var easing = jQuery.easing.easeOutCubic !== undefined ? 'easeOutCubic' : 'linear';
                var viewportHeight = window.innerHeight;
                var headerHeight = 165;

                jQuery('html, body').animate({
                    scrollTop: (headerHeight + elementTop - viewportHeight / 2)
                }, 250, easing);
            }
        };

        var setIframeHeight = function (documentHeight) {
            $iframe.height(documentHeight);
        };

        jQuery(function () {
            $iframe = jQuery('#landingIframe');

            eventer(messageEvent, function(event) {
                var data = event.data.split('|');

                setIframeHeight(parseInt(data[1]));
                scrollTo(parseInt(data[0]));
            },false);
        });

    </script>
    <iframe src="index.php" width="100%" height="2550" border="0" style="border:0;" id="landingIframe"></iframe>
